package pokedex;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import javax.swing.JOptionPane;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class LoadingThread implements Runnable {
	int start, end;
	List<Pokemon> pokemons;
	

	
   public LoadingThread(int start, int end, List<Pokemon> pokemons) {
		super();
		this.start = start;
		this.end = end;
		this.pokemons = pokemons;
	}


public void run() {
	   try {
			
			String link = "https://pokeapi.co/api/v2/";
			System.out.println("DEBUG: Tentando conex�o com a API");
			JSONObject json = connect(link);
			System.out.println("DEBUG: Conex�o estabelecida");
			json = connect(json.get("pokemon").toString());
			JSONArray pokemonsList = json.getJSONArray("results");
			System.out.println("DEBUG: carregando pokemons");
			JSONObject pokemon;
			for(int i = start; i < end; i++) {
				System.out.println("DEBUG: Carregando pokemon " + i);
				int id;
				String name;
				String[] type = new String[2], ab = new String[2];
				int hp = 0, attack = 0, defense = 0, height, weight;
				
				
				pokemon = pokemonsList.getJSONObject(i);
				json = connect(pokemon.getString("url"));
				JSONArray types = json.getJSONArray("types");
				JSONArray abilities = json.getJSONArray("abilities");
				name = pokemon.getString("name");
				id = json.getInt("id");
				for(int j = 0; j < types.length(); j++) {
					if(j < 2) {
						JSONObject JSONtype = types.getJSONObject(j).getJSONObject("type");
						type[j] = JSONtype.getString("name");
					}
				}
				for(int j = 0; j < abilities.length(); j++) {
					if(j < 2) {
						JSONObject JSONtype = abilities.getJSONObject(j).getJSONObject("ability");
						ab[j] = JSONtype.getString("name");
					}
				}
				JSONArray stats = json.getJSONArray("stats");
				for(int j = 0; j < stats.length(); j++) {
					JSONObject currentStat = stats.getJSONObject(j);
					JSONObject stat = currentStat.getJSONObject("stat");
					String statName = stat.getString("name");
					if(statName.equals("hp")) {
						hp = currentStat.getInt("base_stat");
					}
					if(statName.equals("attack")) {
						attack = currentStat.getInt("base_stat");
					}
					if(statName.equals("defense")) {
						defense = currentStat.getInt("base_stat");
					}
					
				}
				height = json.getInt("height");
				weight = json.getInt("weight");

				Pokemon novoPokemon = new Pokemon(id, name, type[0], type[1], hp, attack, defense, height, weight, ab[0], ab[1]);
				pokemons.add(novoPokemon);
				
			}
		}
		catch(Exception e) {
		}
   }
   
	
	public static JSONObject connect(String link) throws IOException, JSONException {
		URL url = new URL(link);
		HttpURLConnection connection = (HttpURLConnection)url.openConnection();
		connection.setRequestProperty("User-Agent", "Mozilla/5.0");
		connection.connect();
		BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
		StringBuilder sb = new StringBuilder();
		String line;
		while((line = in.readLine()) != null) {
			sb.append(line);
		}
		JSONObject json = new JSONObject(sb.toString());
		return json;
	}
}