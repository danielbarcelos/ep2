package pokedex;

import java.awt.Component;
import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.FlowLayout;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.GridLayout;
import javax.swing.JTextField;
import javax.swing.ListModel;
import javax.swing.SwingConstants;
import javax.swing.JSpinner;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.JList;
import javax.swing.AbstractListModel;
import javax.swing.event.ListSelectionListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import javax.swing.event.ListSelectionEvent;
import java.beans.PropertyChangeListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.beans.PropertyChangeEvent;
import java.awt.event.InputMethodListener;
import java.awt.event.InputMethodEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.Font;
import java.awt.Color;

public class Main {

	private JFrame frame;
	private JTextField nomeTreinador;
	private JTextField idTreinador;
	private int id=0;
	
	List<Pokedex> pokedexes = new ArrayList<Pokedex>();
	static List<Pokemon> pokemons = new ArrayList<Pokemon>();
	List<Pokemon> pokemonsSelecionados = new ArrayList<Pokemon>();
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		JOptionPane.showMessageDialog(null, "Os pokemons est�o sendo carregados.", "Carregando...", JOptionPane.INFORMATION_MESSAGE);
			Runnable r = new LoadingThread(0,49, pokemons);
			new Thread(r).start();
			Runnable r1 = new LoadingThread(50, 100, pokemons);
			new Thread(r1).start();
			Runnable r2 = new LoadingThread(101, 150, pokemons);
			new Thread(r2).start();
			Runnable r3 = new LoadingThread(151, 200, pokemons);
			new Thread(r3).start();
			Runnable r4 = new LoadingThread(201, 250, pokemons);
			new Thread(r4).start();
			Runnable r5 = new LoadingThread(251, 300, pokemons);
			new Thread(r5).start();
			Runnable r6 = new LoadingThread(301, 350, pokemons);
			new Thread(r6).start();
			Runnable r7 = new LoadingThread(351, 400, pokemons);
			new Thread(r7).start();
			Runnable r8 = new LoadingThread(401, 450, pokemons);
			new Thread(r8).start();
			Runnable r9 = new LoadingThread(451, 500, pokemons);
			new Thread(r9).start();
			Runnable r10 = new LoadingThread(501, 550, pokemons);
			new Thread(r10).start();
			Runnable r11 = new LoadingThread(551, 600, pokemons);
			new Thread(r11).start();
			Runnable r12 = new LoadingThread(601, 650, pokemons);
			new Thread(r12).start();
			Runnable r13 = new LoadingThread(651, 700, pokemons);
			new Thread(r13).start();
			Runnable r14 = new LoadingThread(701, 750, pokemons);
			new Thread(r14).start();
			Runnable r15 = new LoadingThread(751, 800, pokemons);
			new Thread(r15).start();
			Runnable r16 = new LoadingThread(801, 850, pokemons);
			new Thread(r16).start();
			Runnable r17 = new LoadingThread(851, 900, pokemons);
			new Thread(r17).start();
			Runnable r18 = new LoadingThread(901, 949, pokemons);
			new Thread(r18).start();
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Main window = new Main();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Main() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		JPanel painelVazio = new JPanel();
		frame.setBounds(100, 100, 600, 600);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JButton btnSalvar = new JButton("Salvar");
		btnSalvar.setEnabled(false);
		JPanel painelMenu = new JPanel();
		painelMenu.setBounds(12, 34, 205, 527);
		frame.getContentPane().add(painelMenu);
		painelMenu.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		JPanel painelCriarTreinador = new JPanel();
		painelCriarTreinador.setBounds(229, 34, 359, 527);
		frame.getContentPane().add(painelCriarTreinador);
		painelCriarTreinador.setLayout(new GridLayout(5, 2, 0, 0));

		JPanel panel_1 = new JPanel();
		painelCriarTreinador.add(panel_1);
		panel_1.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Nome");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setBounds(0, 11, 159, 14);
		panel_1.add(lblNewLabel);
		
		JPanel panel = new JPanel();
		painelCriarTreinador.add(panel);
		panel.setLayout(null);
		
		nomeTreinador = new JTextField();
		nomeTreinador.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {
				if(nomeTreinador.getText()==null || nomeTreinador.getText()=="") {
					btnSalvar.setEnabled(false);
				}
				else{
					btnSalvar.setEnabled(true);
				}
			}
		});
		nomeTreinador.setBounds(10, 11, 159, 20);
		panel.add(nomeTreinador);
		nomeTreinador.setColumns(10);
		
		JPanel panel_3 = new JPanel();
		painelCriarTreinador.add(panel_3);
		panel_3.setLayout(null);
		
		JLabel lblNewLabel_1 = new JLabel("Idade");
		lblNewLabel_1.setBounds(66, 45, 46, 14);
		panel_3.add(lblNewLabel_1);
		
		JPanel panel_2 = new JPanel();
		painelCriarTreinador.add(panel_2);
		panel_2.setLayout(null);
		
		JSpinner idadeTreinador = new JSpinner();
		idadeTreinador.setModel(new SpinnerNumberModel(0, 0, 200, 1));
		idadeTreinador.setBounds(10, 42, 159, 20);
		panel_2.add(idadeTreinador);
		
		JPanel panel_5 = new JPanel();
		painelCriarTreinador.add(panel_5);
		panel_5.setLayout(null);
		
		JLabel lblNewLabel_2 = new JLabel("Sexo");
		lblNewLabel_2.setBounds(66, 45, 46, 14);
		panel_5.add(lblNewLabel_2);
		
		JPanel panel_4 = new JPanel();
		painelCriarTreinador.add(panel_4);
		panel_4.setLayout(null);
		
		JComboBox sexoTreinador = new JComboBox();
		sexoTreinador.setModel(new DefaultComboBoxModel(new String[] {"Feminino", "Masculino", "Outros"}));
		sexoTreinador.setBounds(10, 42, 159, 20);
		panel_4.add(sexoTreinador);
		
		JPanel panel_6 = new JPanel();
		painelCriarTreinador.add(panel_6);
		panel_6.setLayout(null);
		
		JLabel lblId = new JLabel("ID");
		lblId.setBounds(66, 45, 46, 14);
		panel_6.add(lblId);
		
		JPanel panel_7 = new JPanel();
		painelCriarTreinador.add(panel_7);
		panel_7.setLayout(null);
		
		idTreinador = new JTextField();
		idTreinador.setEditable(false);
		idTreinador.setBounds(10, 42, 159, 20);
		panel_7.add(idTreinador);
		idTreinador.setColumns(10);
		idTreinador.setText(Integer.toString(id));
		
		JPanel panel_8 = new JPanel();
		painelCriarTreinador.add(panel_8);
		panel_8.setLayout(null);
		
		JButton btnEscolherPokemons = new JButton("Escolher Pokemons");
		btnEscolherPokemons.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				pokemonsSelecionados.clear();
				EventQueue.invokeLater(new Runnable() {
					public void run() {
						try {
							EscolherPokemon window = new EscolherPokemon(pokemons, pokemonsSelecionados);
							window.frame.setVisible(true);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
			}
			
		});
		btnEscolherPokemons.setBounds(28, 41, 123, 23);
		panel_8.add(btnEscolherPokemons);
		
		JPanel panel_9 = new JPanel();
		painelCriarTreinador.add(panel_9);
		panel_9.setLayout(null);
		
		btnSalvar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String nome = nomeTreinador.getText();
				int idade = (int) idadeTreinador.getValue();
				int idNovo = Integer.parseInt(idTreinador.getText());
				char sexo='O';
				switch(sexoTreinador.getSelectedIndex()) {
				case 0: {
					sexo = 'F';
					break;
				}
				case 1: {
					sexo = 'M';
					break;
				}
				}
				
				Treinador novoTreinador=new Treinador(nome, idade, sexo, idNovo);
				Pokedex novaPokedex = new Pokedex(novoTreinador, pokemonsSelecionados);
				pokedexes.add(novaPokedex);
				
				nomeTreinador.setText("");
				idadeTreinador.setValue(0);
				sexoTreinador.setSelectedIndex(0);
				id++;
				painelVazio.setVisible(true);
				painelCriarTreinador.setVisible(false);
				btnSalvar.setEnabled(false);
			}
		});
		JList listTreinadores = new JList();
		btnSalvar.setBounds(0, 41, 179, 23);
		panel_9.add(btnSalvar);
		painelCriarTreinador.setVisible(false);
		JButton btnDetalhar = new JButton("Ver Detalhes");
		btnDetalhar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				EventQueue.invokeLater(new Runnable() {
					public void run() {
						try {
							Pokedex pokedexSelecionada = new Pokedex(null, null);
							String model = (String) listTreinadores.getSelectedValue();
							String id = model.substring(0, model.indexOf(' '));
							int intId = Integer.parseInt(id);
							for(Pokedex pokedex : pokedexes) {
								if(pokedex.getTreinador().getId()==intId) {
									pokedexSelecionada = pokedex;
									break;
								}
							}
							DetalhesTreinador window = new DetalhesTreinador(pokedexSelecionada);
							window.frame.setVisible(true);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
			}
		});
		
		JPanel painelVerTreinadores = new JPanel();
		painelVerTreinadores.setBounds(229, 34, 359, 527);
		frame.getContentPane().add(painelVerTreinadores);
		painelVerTreinadores.setLayout(null);
		
		JPanel panel_10 = new JPanel();
		panel_10.setBounds(0, 0, 349, 421);
		painelVerTreinadores.add(panel_10);
		panel_10.setLayout(new GridLayout(1, 1, 0, 0));
		
		listTreinadores.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent arg0) {
				btnDetalhar.setEnabled(true);
			}
		});
		listTreinadores.setModel(new AbstractListModel() {
			String[] values = new String[] {};
			public int getSize() {
				return values.length;
			}
			public Object getElementAt(int index) {
				return values[index];
			}
		});
		panel_10.add(listTreinadores);
		
		JPanel panel_11 = new JPanel();
		panel_11.setBounds(0, 422, 349, 105);
		painelVerTreinadores.add(panel_11);
		panel_11.setLayout(null);
		
		btnDetalhar.setEnabled(false);
		btnDetalhar.setBounds(187, 41, 152, 23);
		panel_11.add(btnDetalhar);
		painelVerTreinadores.setVisible(false);
		painelVazio.setBounds(0, 0, 10, 10);
		frame.getContentPane().add(painelVazio);
		
		JButton criarTreinador = new JButton("Cadastrar Treinador");
		criarTreinador.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				idTreinador.setText(Integer.toString(id));
				painelCriarTreinador.setVisible(true);
				painelVazio.setVisible(false);
				painelVerTreinadores.setVisible(false);
			}
		});
		painelMenu.add(criarTreinador);
		
		JButton btnVerTreinadores = new JButton("Ver Treinadores");
		btnVerTreinadores.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				DefaultListModel<String> model = new DefaultListModel<>();
				for(Pokedex pokedex : pokedexes) {
					String linha = pokedex.getTreinador().getId() + " - " + pokedex.getTreinador().getNome();
					model.addElement(linha);
				}
				listTreinadores.setModel(model);	
				
				painelCriarTreinador.setVisible(false);
				painelVazio.setVisible(false);
				painelVerTreinadores.setVisible(true);
			}
		});
		painelMenu.add(btnVerTreinadores);
		
		JButton btnBuscarPokemons = new JButton("Buscar Pokemons");
		btnBuscarPokemons.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				EventQueue.invokeLater(new Runnable() {
					public void run() {
						try {
							BuscarPokemon window = new BuscarPokemon(pokemons);
							window.frame.setVisible(true);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
			}
		});
		painelMenu.add(btnBuscarPokemons);
	}

}
