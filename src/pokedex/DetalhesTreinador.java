package pokedex;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.GridLayout;
import javax.swing.JPanel;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JList;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.ListSelectionEvent;

public class DetalhesTreinador {

	JFrame frame;

	Pokedex pokedex;

	/**
	 * Create the application.
	 */
	public DetalhesTreinador(Pokedex pokedex) {
		this.pokedex = pokedex;
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(new GridLayout(1, 2, 0, 0));
		
		JPanel panel = new JPanel();
		frame.getContentPane().add(panel);
		panel.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Nome");
		lblNewLabel.setBounds(43, 5, 27, 14);
		panel.add(lblNewLabel);
		
		JLabel nome = new JLabel(pokedex.getTreinador().getNome());
		nome.setBounds(100, 5, 107, 14);
		panel.add(nome);
		
		JLabel lblNewLabel_2 = new JLabel("Idade");
		lblNewLabel_2.setBounds(43, 30, 46, 14);
		panel.add(lblNewLabel_2);
		
		JLabel idade = new JLabel(Integer.toString(pokedex.getTreinador().getIdade()));
		idade.setBounds(100, 30, 107, 14);
		panel.add(idade);
		
		JLabel lblSexo = new JLabel("Sexo");
		lblSexo.setBounds(43, 55, 46, 14);
		panel.add(lblSexo);
		String sexoTreinador = "";
		switch(pokedex.getTreinador().getSexo()) {
		case 'M':{
			sexoTreinador = "Masculino";
			break;
		}
		case 'F':{
			sexoTreinador = "Feminino";
			break;
		}
		default:{
			sexoTreinador = "Outros";
		}
		}
		JLabel sexo = new JLabel(sexoTreinador);
		sexo.setBounds(100, 55, 107, 14);
		panel.add(sexo);
		
		JLabel lblNewLabel_5 = new JLabel("ID");
		lblNewLabel_5.setBounds(43, 83, 46, 14);
		panel.add(lblNewLabel_5);
		
		JLabel id = new JLabel(Integer.toString(pokedex.getTreinador().getId()));
		id.setBounds(99, 83, 108, 14);
		panel.add(id);
		
		JButton btnFechar = new JButton("Fechar");
		btnFechar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				frame.dispose();
			}
		});
		btnFechar.setBounds(64, 227, 89, 23);
		panel.add(btnFechar);
		
		JPanel panel_1 = new JPanel();
		frame.getContentPane().add(panel_1);
		panel_1.setLayout(null);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBounds(0, 0, 217, 220);
		panel_1.add(panel_2);
		panel_2.setLayout(new GridLayout(1, 1, 0, 0));
		JButton btnVerDetalhes = new JButton("Ver Detalhes");
		JList list = new JList();
		list.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent arg0) {
				btnVerDetalhes.setEnabled(true);
			}
		});
		panel_2.add(list);
		
		btnVerDetalhes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				verDetalhes(pokedex.getPokemons().get(list.getSelectedIndex()));
			}
		});
		btnVerDetalhes.setEnabled(false);
		btnVerDetalhes.setBounds(48, 231, 121, 23);
		panel_1.add(btnVerDetalhes);
		DefaultListModel<String> model = new DefaultListModel<>();
		for(Pokemon pokemon : pokedex.getPokemons()) {
			String linha = pokemon.getName();
			model.addElement(linha);
		}
		list.setModel(model);
	}
	private void verDetalhes(Pokemon pokemon) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					DetalhesPokemon frame = new DetalhesPokemon(pokemon);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}

/**
 * criar 
 * 
 * 
 * */
