package pokedex;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.GridLayout;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.swing.JLabel;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;

public class DetalhesPokemon extends JFrame {

	JPanel contentPane;
	
	private Pokemon pokemon;

	public DetalhesPokemon (Pokemon pokemon) {
		this.pokemon = pokemon;
		load();
	}

	/**
	 * Create the frame.
	 */
	private void load() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 270, 332);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new GridLayout(0, 1, 0, 0));
		
		JPanel panel = new JPanel();
		contentPane.add(panel);
		panel.setLayout(new GridLayout(0, 1, 0, 0));
		
		JPanel picPanel = new JPanel();
		panel.add(picPanel);
		picPanel.setLayout(new GridLayout(1, 1, 0, 0));
		
		JPanel panel_2 = new JPanel();
		panel.add(panel_2);
		panel_2.setLayout(new GridLayout(6, 1, 0, 0));
		
		JPanel panel_3 = new JPanel();
		panel_3.setLayout(null);
		panel_2.add(panel_3);
		
		JLabel label = new JLabel("Nome:");
		label.setBounds(10, 2, 46, 14);
		panel_3.add(label);
		
		JLabel lblNomelbl = new JLabel(" ");
		lblNomelbl.setBounds(50, 2, 84, 14);
		panel_3.add(lblNomelbl);
		
		JLabel label_2 = new JLabel("ID:");
		label_2.setBounds(123, 0, 46, 14);
		panel_3.add(label_2);
		
		JLabel lblIdlbl = new JLabel(" ");
		lblIdlbl.setBounds(150, 0, 46, 14);
		panel_3.add(lblIdlbl);
		
		JPanel panel_4 = new JPanel();
		panel_4.setLayout(null);
		panel_2.add(panel_4);
		
		JLabel label_4 = new JLabel("HP:");
		label_4.setBounds(10, 0, 46, 14);
		panel_4.add(label_4);
		
		JLabel lblHplbl = new JLabel(" ");
		lblHplbl.setBounds(29, 0, 46, 14);
		panel_4.add(lblHplbl);
		
		JLabel label_6 = new JLabel("ATK:");
		label_6.setBounds(66, 0, 46, 14);
		panel_4.add(label_6);
		
		JLabel lblAtklbl = new JLabel(" ");
		lblAtklbl.setBounds(91, 0, 46, 14);
		panel_4.add(lblAtklbl);
		
		JLabel label_8 = new JLabel("DEF:");
		label_8.setBounds(123, 0, 46, 14);
		panel_4.add(label_8);
		
		JLabel lblDeflbl = new JLabel(" ");
		lblDeflbl.setBounds(150, 0, 46, 14);
		panel_4.add(lblDeflbl);
		
		JPanel panel_5 = new JPanel();
		panel_5.setLayout(null);
		panel_2.add(panel_5);
		
		JLabel label_10 = new JLabel("Tipo 1:");
		label_10.setBounds(10, 0, 46, 14);
		panel_5.add(label_10);
		
		JLabel lblTypelbl = new JLabel(" ");
		lblTypelbl.setBounds(49, 0, 46, 14);
		panel_5.add(lblTypelbl);
		
		JLabel label_12 = new JLabel("Tipo 2:");
		label_12.setBounds(123, 0, 46, 14);
		panel_5.add(label_12);
		
		JLabel lblTypelbl_1 = new JLabel(" ");
		lblTypelbl_1.setBounds(161, 0, 46, 14);
		panel_5.add(lblTypelbl_1);
		
		JPanel panel_6 = new JPanel();
		panel_6.setLayout(null);
		panel_2.add(panel_6);
		
		JLabel label_14 = new JLabel("Habilidade 1:");
		label_14.setBounds(10, 0, 80, 14);
		panel_6.add(label_14);
		
		JLabel lblAblbl = new JLabel(" ");
		lblAblbl.setBounds(85, 0, 124, 14);
		panel_6.add(lblAblbl);
		
		JPanel panel_7 = new JPanel();
		panel_7.setLayout(null);
		panel_2.add(panel_7);
		
		JLabel label_16 = new JLabel("Habilidade 2:");
		label_16.setBounds(10, 0, 80, 14);
		panel_7.add(label_16);
		
		JLabel lblAblbl_1 = new JLabel(" ");
		lblAblbl_1.setBounds(85, 0, 124, 14);
		panel_7.add(lblAblbl_1);
		
		JPanel panel_8 = new JPanel();
		panel_8.setLayout(null);
		panel_2.add(panel_8);
		
		JLabel label_18 = new JLabel("Altura:");
		label_18.setBounds(10, 0, 46, 14);
		panel_8.add(label_18);
		
		JLabel label_19 = new JLabel("Peso:");
		label_19.setBounds(124, 0, 46, 14);
		panel_8.add(label_19);
		
		JLabel lblHeightlbl = new JLabel(" ");
		lblHeightlbl.setBounds(47, 0, 67, 14);
		panel_8.add(lblHeightlbl);
		
		JLabel lblWeightlbl = new JLabel(" ");
		lblWeightlbl.setBounds(161, 0, 46, 14);
		panel_8.add(lblWeightlbl);
		
		lblNomelbl.setText(pokemon.getName());
		lblIdlbl.setText(Integer.toString(pokemon.getId()));
		lblAtklbl.setText(Integer.toString(pokemon.getAttack()));
		lblAblbl.setText(pokemon.getAbilitie1());
		lblAblbl_1.setText(pokemon.getAbilitie2());
		lblDeflbl.setText(Integer.toString(pokemon.getDefense()));
		lblTypelbl.setText(pokemon.getType1());
		lblTypelbl_1.setText(pokemon.getType2());
		lblHplbl.setText(Integer.toString(pokemon.getHp()));
		lblWeightlbl.setText(Integer.toString(pokemon.getWeight()));
		lblHeightlbl.setText(Integer.toString(pokemon.getHeight()));
		BufferedImage myPicture;
		Path currentRelativePath = Paths.get("");
		String s = currentRelativePath.toAbsolutePath().toString();
		try {
			myPicture = ImageIO.read(new File(s + "/data/images/" + pokemon.getName() + ".png"));
			JLabel picLabel = new JLabel(new ImageIcon(myPicture));
			picPanel.removeAll();
			picPanel.add(picLabel);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
