package pokedex;

import java.util.ArrayList;
import java.util.List;

public class Pokedex {
	private Treinador treinador;
	private List<Pokemon> pokemons;
	
	public Pokedex(Treinador treinador, List<Pokemon> pokemons) {
		super();
		this.treinador = treinador;
		this.pokemons = pokemons;
	}

	public Treinador getTreinador() {
		return treinador;
	}

	public void setTreinador(Treinador treinador) {
		this.treinador = treinador;
	}

	public List<Pokemon> getPokemons() {
		return pokemons;
	}

	public void setPokemons(List<Pokemon> pokemons) {
		this.pokemons = pokemons;
	}

	public void addPokemon(Pokemon pokemon) {
		this.pokemons.add(pokemon);
	}
}
