package pokedex;

public class Pokemon {
	private int id;
	private String name;
	private String type1;
	private String type2;
	private int hp;
	private int attack;
	private int defense;
	private int height;
	private int weight;
	private String abilitie1;
	private String abilitie2;
	
	public Pokemon() {}
	
	public Pokemon(int id, String name, String type1, String type2, int hp, int attack, int defense, int height,
			int weight, String abilitie1, String abilitie2) {
		this.id = id;
		this.name = name;
		this.type1 = type1;
		this.type2 = type2;
		this.hp = hp;
		this.attack = attack;
		this.defense = defense;
		this.height = height;
		this.weight = weight;
		this.abilitie1 = abilitie1;
		this.abilitie2 = abilitie2;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType1() {
		return type1;
	}

	public void setType1(String type1) {
		this.type1 = type1;
	}

	public String getType2() {
		return type2;
	}

	public void setType2(String type2) {
		this.type2 = type2;
	}

	public int getHp() {
		return hp;
	}

	public void setHp(int hp) {
		this.hp = hp;
	}

	public int getAttack() {
		return attack;
	}

	public void setAttack(int attack) {
		this.attack = attack;
	}

	public int getDefense() {
		return defense;
	}

	public void setDefense(int defense) {
		this.defense = defense;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

	public String getAbilitie1() {
		return abilitie1;
	}

	public void setAbilitie1(String abilitie1) {
		this.abilitie1 = abilitie1;
	}

	public String getAbilitie2() {
		return abilitie2;
	}

	public void setAbilitie2(String abilitie2) {
		this.abilitie2 = abilitie2;
	}
	
		
}
