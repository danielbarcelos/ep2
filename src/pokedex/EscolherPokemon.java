package pokedex;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.GridLayout;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JLabel;
import javax.imageio.ImageIO;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.ListSelectionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class EscolherPokemon {

	JFrame frame;

	List<Pokemon> pokemons;
	List<Pokemon> pokemonsSelecionados;

	/**
	 * Create the application.
	 */
	public EscolherPokemon(List<Pokemon> pokemons, List<Pokemon> pokemonsSelecionados) {
		this.pokemons = pokemons;
		this.pokemonsSelecionados = pokemonsSelecionados;
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(new GridLayout(1, 2, 0, 0));

		JList pokemonList = new JList();

		frame.getContentPane().add(new JScrollPane(pokemonList));
		
		JPanel panel = new JPanel();
		frame.getContentPane().add(panel);
		panel.setLayout(new GridLayout(0, 1, 0, 0));
		
		JPanel picPanel = new JPanel();
		panel.add(picPanel);
		picPanel.setLayout(new GridLayout(1, 1, 0, 0));
		
		JPanel panel_1 = new JPanel();
		panel.add(panel_1);
		panel_1.setLayout(new GridLayout(7, 1, 0, 0));
		
		JPanel panel_4 = new JPanel();
		panel_1.add(panel_4);
		panel_4.setLayout(null);
		
		JLabel lblNome = new JLabel("Nome:");
		lblNome.setBounds(10, 2, 46, 14);
		panel_4.add(lblNome);
		
		JLabel lblNomelbl = new JLabel(" ");
		lblNomelbl.setBounds(50, 2, 84, 14);
		panel_4.add(lblNomelbl);
		
		JLabel lblId = new JLabel("ID:");
		lblId.setBounds(123, 0, 46, 14);
		panel_4.add(lblId);
		
		JLabel lblIdlbl = new JLabel(" ");
		lblIdlbl.setBounds(150, 0, 46, 14);
		panel_4.add(lblIdlbl);
		
		JPanel panel_3 = new JPanel();
		panel_1.add(panel_3);
		panel_3.setLayout(null);
		
		JLabel lblHp = new JLabel("HP:");
		lblHp.setBounds(10, 0, 46, 14);
		panel_3.add(lblHp);
		
		JLabel lblHplbl = new JLabel(" ");
		lblHplbl.setBounds(29, 0, 46, 14);
		panel_3.add(lblHplbl);
		
		JLabel lblAtk = new JLabel("ATK:");
		lblAtk.setBounds(66, 0, 46, 14);
		panel_3.add(lblAtk);
		
		JLabel lblAtklbl = new JLabel(" ");
		lblAtklbl.setBounds(91, 0, 46, 14);
		panel_3.add(lblAtklbl);
		
		JLabel lblDef = new JLabel("DEF:");
		lblDef.setBounds(123, 0, 46, 14);
		panel_3.add(lblDef);
		
		JLabel lblDeflbl = new JLabel(" ");
		lblDeflbl.setBounds(150, 0, 46, 14);
		panel_3.add(lblDeflbl);
		
		JPanel panel_5 = new JPanel();
		panel_1.add(panel_5);
		panel_5.setLayout(null);
		
		JLabel lblTipo = new JLabel("Tipo 1:");
		lblTipo.setBounds(10, 0, 46, 14);
		panel_5.add(lblTipo);
		
		JLabel lblTypelbl = new JLabel(" ");
		lblTypelbl.setBounds(49, 0, 46, 14);
		panel_5.add(lblTypelbl);
		
		JLabel lblTipo_1 = new JLabel("Tipo 2:");
		lblTipo_1.setBounds(123, 0, 46, 14);
		panel_5.add(lblTipo_1);
		
		JLabel lblTypelbl_1 = new JLabel(" ");
		lblTypelbl_1.setBounds(161, 0, 46, 14);
		panel_5.add(lblTypelbl_1);
		
		JPanel panel_6 = new JPanel();
		panel_1.add(panel_6);
		panel_6.setLayout(null);
		
		JLabel lblHabilidade = new JLabel("Habilidade 1:");
		lblHabilidade.setBounds(10, 0, 80, 14);
		panel_6.add(lblHabilidade);
		
		JLabel lblAblbl = new JLabel(" ");
		lblAblbl.setBounds(85, 0, 124, 14);
		panel_6.add(lblAblbl);
		
		JPanel panel_7 = new JPanel();
		panel_1.add(panel_7);
		panel_7.setLayout(null);
		
		JLabel lblHabilidade_1 = new JLabel("Habilidade 2:");
		lblHabilidade_1.setBounds(10, 0, 80, 14);
		panel_7.add(lblHabilidade_1);
		
		JLabel lblAblbl_1 = new JLabel(" ");
		lblAblbl_1.setBounds(85, 0, 124, 14);
		panel_7.add(lblAblbl_1);
		
		JPanel panel_8 = new JPanel();
		panel_1.add(panel_8);
		panel_8.setLayout(null);
		
		JLabel lblAltura = new JLabel("Altura:");
		lblAltura.setBounds(10, 0, 46, 14);
		panel_8.add(lblAltura);
		
		JLabel lblPeso = new JLabel("Peso:");
		lblPeso.setBounds(124, 0, 46, 14);
		panel_8.add(lblPeso);
		
		JLabel lblHeightlbl = new JLabel(" ");
		lblHeightlbl.setBounds(47, 0, 67, 14);
		panel_8.add(lblHeightlbl);
		
		JLabel lblWeightlbl = new JLabel(" ");
		lblWeightlbl.setBounds(161, 0, 46, 14);
		panel_8.add(lblWeightlbl);
		
		JPanel panel_9 = new JPanel();
		panel_1.add(panel_9);
		panel_9.setLayout(null);
		
		JButton btnEscolherPokemon = new JButton("Escolher Pokemon");
		btnEscolherPokemon.setEnabled(false);
		btnEscolherPokemon.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				pokemonsSelecionados.add(pokemons.get(pokemonList.getSelectedIndex()));
				JOptionPane.showMessageDialog(null, "Pokemon adicionado com sucesso", "SUCESSO", JOptionPane.INFORMATION_MESSAGE);
			}
		});
		btnEscolherPokemon.setBounds(49, 0, 119, 18);
		panel_9.add(btnEscolherPokemon);
		
		DefaultListModel<String> model = new DefaultListModel<>();
		for(Pokemon pokemon : pokemons) {
			String linha = pokemon.getName();
			model.addElement(linha);
		}
		pokemonList.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent arg0) {
				int i = pokemonList.getSelectedIndex();
				lblNomelbl.setText(pokemons.get(i).getName());
				lblIdlbl.setText(Integer.toString(pokemons.get(i).getId()));
				lblAtklbl.setText(Integer.toString(pokemons.get(i).getAttack()));
				lblAblbl.setText(pokemons.get(i).getAbilitie1());
				lblAblbl_1.setText(pokemons.get(i).getAbilitie2());
				lblDeflbl.setText(Integer.toString(pokemons.get(i).getDefense()));
				lblTypelbl.setText(pokemons.get(i).getType1());
				lblTypelbl_1.setText(pokemons.get(i).getType2());
				lblHplbl.setText(Integer.toString(pokemons.get(i).getHp()));
				lblWeightlbl.setText(Integer.toString(pokemons.get(i).getWeight()));
				lblHeightlbl.setText(Integer.toString(pokemons.get(i).getHeight()));
				BufferedImage myPicture;
				btnEscolherPokemon.setEnabled(true);
				Path currentRelativePath = Paths.get("");
				String s = currentRelativePath.toAbsolutePath().toString();
				try {
					myPicture = ImageIO.read(new File(s + "/data/images/" + pokemons.get(i).getName() + ".png"));
					JLabel picLabel = new JLabel(new ImageIcon(myPicture));
					picPanel.removeAll();
					picPanel.add(picLabel);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		pokemonList.setModel(model);	
	}
}
