package pokedex;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JSeparator;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.awt.event.ActionEvent;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.TitledBorder;

import com.sun.org.apache.xalan.internal.xsltc.dom.AbsoluteIterator;

import javax.swing.UIManager;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import javax.swing.JRadioButton;
import java.awt.GridLayout;

import javax.imageio.ImageIO;
import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.GroupLayout.Alignment;

public class BuscarPokemon {

	JFrame frame;
	private JTextField nomePokemon;
	private JTextField tipoPokemon;
	private List<Pokemon> pokemons;
	private List<Pokemon> pokemonsFiltrados;
	private JPanel panel = new JPanel();
	/**
	 * Create the application.
	 */
	public BuscarPokemon(List<Pokemon> pokemons) {
		this.pokemons = pokemons;
		this.pokemonsFiltrados = new ArrayList<Pokemon>(pokemons);
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 669, 389);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		nomePokemon = new JTextField();
		nomePokemon.setBounds(98, 11, 261, 20);
		frame.getContentPane().add(nomePokemon);
		nomePokemon.setColumns(10);
		
		JButton btnBuscar = new JButton("Buscar por Nome");
		btnBuscar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				pokemonsFiltrados.clear();
				int i;
				for(i=0; i<pokemons.size(); i++) {
					Pokemon pokemon = pokemons.get(i);
					if(pokemon.getName().contains(nomePokemon.getText())) {
						pokemonsFiltrados.add(pokemon);
					}
				}
				adicionarPokemon();
			}
		});
		btnBuscar.setBounds(369, 10, 274, 23);
		frame.getContentPane().add(btnBuscar);
		
		tipoPokemon = new JTextField();
		tipoPokemon.setBounds(98, 42, 261, 20);
		frame.getContentPane().add(tipoPokemon);
		tipoPokemon.setColumns(10);
		
		JButton btnBuscarPorTipo = new JButton("Buscar por Tipo");
		btnBuscarPorTipo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				pokemonsFiltrados.clear();
				int i;
				for(i=0; i<pokemons.size(); i++) {
					Pokemon pokemon = pokemons.get(i);
					if((pokemon.getType1() != null && pokemon.getType1().contains(tipoPokemon.getText())) || (pokemon.getType2() != null && pokemon.getType2().contains(tipoPokemon.getText()))) {
						pokemonsFiltrados.add(pokemon);
						
					}
					
				}
				adicionarPokemon();
			}
		});
		btnBuscarPorTipo.setBounds(369, 44, 274, 23);
		frame.getContentPane().add(btnBuscarPorTipo);
		
		JLabel lblDigiteONome = new JLabel("Digite o Nome:");
		lblDigiteONome.setBounds(10, 14, 82, 14);
		frame.getContentPane().add(lblDigiteONome);
		
		JLabel lblDigiteOTipo = new JLabel("Digite o Tipo:");
		lblDigiteOTipo.setBounds(10, 45, 82, 14);
		frame.getContentPane().add(lblDigiteOTipo);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(10, 76, 633, -4);
		frame.getContentPane().add(separator);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setBounds(10, 83, 633, 256);
		frame.getContentPane().add(scrollPane);
		panel.setLayout(new GridLayout(0,4,5,5));
		panel.setBackground(Color.WHITE);
		adicionarPokemon();
		scrollPane.setViewportView(panel);

	}
	
	private void adicionarPokemon() {
		panel.removeAll();
		for(Pokemon pokemon: pokemonsFiltrados) {
			JPanel panel_1 = new JPanel();
			panel_1.setPreferredSize(new Dimension(150,150));
			panel.add(panel_1);
			
			panel_1.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent arg0) {
					verDetalhes(pokemon);
				}
			});
			panel_1.setLayout(null);
			
			JPanel picturePanel = new JPanel();
			picturePanel.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent arg0) {
					verDetalhes(pokemon);
				}
			});
			picturePanel.setBounds(10, 11, 130, 100);
			panel_1.add(picturePanel);
			BufferedImage myPicture;
			Path currentRelativePath = Paths.get("");
			String s = currentRelativePath.toAbsolutePath().toString();
			try {
				myPicture = ImageIO.read(new File(s + "/data/images/" + pokemon.getName() + ".png"));
				JLabel picLabel = new JLabel(new ImageIcon(myPicture));
				picturePanel.removeAll();
				picturePanel.add(picLabel);
				picLabel.addMouseListener(new MouseAdapter() {
					@Override
					public void mouseClicked(MouseEvent arg0) {
						verDetalhes(pokemon);
					}
				});
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
			JLabel lblNome = new JLabel(pokemon.getName());
			lblNome.setHorizontalAlignment(SwingConstants.CENTER);
			lblNome.setBounds(10, 122, 130, 14);
			panel_1.add(lblNome);
			lblNome.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent arg0) {
					verDetalhes(pokemon);
				}
			});
		}
		panel.revalidate();
		panel.repaint();
	}
	
	private void verDetalhes(Pokemon pokemon) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					DetalhesPokemon frame = new DetalhesPokemon(pokemon);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}
