O projeto foi feito com a IDE Eclipse.

## Pokedex

Uma pokedex consiste resumidamente em uma agenda, na qual o usu�rio poder� obter informa��es de seus pokemons e de outros, adicionar pokemons � mesma e visualizar a pokedex de outros usu�rios.

## Menu Inicial

No menu inicial o usu�rio poder� realizar seu cadastro, visualizar os treinadores ou buscar os pokemons.

## Tela de Cadastro

Na tela de cadastro o usu�rio ir� preencher o seu nome, idade, sexo e poder� escolher os pokemons que estar�o em sua pokedex. O programa ir� fornecer um ID para o usu�rio.

## Tela de Visualiza��o dos Treinadores

Na tela de visualiza��o dos treinadores haver� uma lista com todos os usu�rios cadastrados e caso se deseje obter informa��es como o nome, sexo, idade, id e a pokedex do treinador basta clicar no bot�o ver detalhes no canto inferior direito.

## Tela de Busca de Pokemons

Na tela de busca o usu�rio poder� obter informa��es dos pokemons de duas maneiras distintas. A primeira consiste em digitar parte do nome ou o nome completo do pokemon e a segunda consiste em digitar o tipo do mesmo.